# Pterodactyl Images

Contains some more generic Docker images for [Pterodactyl](https://pterodactyl.io) that I use as runtime image for some of my own applications / Pterodactyl eggs.
Feel free to use them too!

They're automatically built by Gitlab CI and pushed to Gitlab's Container Registry which you can find on this [Gitlab repo, too](https://gitlab.com/jmgraeffe/pterodactyl-images/container_registry/2117942).